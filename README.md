# Lab

A place to commit the things I'm playing around with. I wouldn't look too closely at most of the repositories here. 

For real projects, take a look [elsewhere](/explore/repos). For old repositories, see [my archive](/archive).
